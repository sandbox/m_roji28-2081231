<?php

function bootstrap_components_entityconnect_alter(&$element){
	// dpm('bootstrap_components_entityconnect_alter');
	// dpm('bootstrap_components_entityconnect_alter');
	// dpm($element,'$element');
	$do = false;
	$element_children = element_children($element); // recursive
	if(!empty($element_children)){
		foreach($element_children as $key){
			$element_children_2 = element_children($element[$key]); // recursive
			if(!empty($element_children_2)){
				foreach($element_children_2 as $key_2){
					if(preg_match('/^add_entityconnect/',$key_2)){
						$do = true;
						$add = $element[$key][$key_2];
						$add_key = $key_2;
					}
					elseif(preg_match('/^edit_entityconnect/',$key_2)){
						$do = true;
						$edit = $element[$key][$key_2];
						$edit_key = $key_2;
					}
					else{
						$field = $element[$key][$key_2];
						$field_key = $key_2;
					}
				}
				if($do){
					$title = bootstrap_components_get_info_element($field);
					$element[$key][$field_key]['#title_display'] = 'none';
					// jika select, maka hapus lagi theme wrapper yang terbentuk karena #pre_render => form_pre_render_conditional_form_element
					if(isset($element[$key][$field_key]['#type']) && $element[$key][$field_key]['#type'] == 'select'){
						unset($element[$key][$field_key]['#theme_wrappers']);
					}
					$element[$key]['#title'] = $title;					
					// $element[$key]['#prefix'] = '<div class="input-group">';
					// $element[$key]['#suffix'] = '</div>';
					$element[$key]['#theme_wrappers'] = array('bootstrap_components_form_element','bootstrap_components_container');
					// $element[$key]['#theme_wrappers'] = 'bootstrap_components_form_element';
					$element[$key]['#bootstrap_components']['no_wrapper'] = true;
					$element[$key]['#bootstrap_components']['input_groups'] = true;
					
					// override
					if(isset($add)){
						$element[$key][$add_key]['#weight'] = 2;
						$element[$key][$add_key]['#attributes']['class'] = array('btn btn-default');
						$element[$key][$add_key]['#prefix'] = '';
						$element[$key][$add_key]['#suffix'] = '';
						$element[$key][$add_key]['#bootstrap_components']['tag_button'] = true;
						$element[$key][$add_key]['#bootstrap_components']['icon'] = '<span class="glyphicon glyphicon-plus"></span>';
					}
					if(isset($edit)){
						$element[$key][$edit_key]['#weight'] = 1;
						$element[$key][$edit_key]['#attributes']['class'] = array('btn btn-default');
						$element[$key][$edit_key]['#prefix'] = '';
						$element[$key][$edit_key]['#suffix'] = '';
						$element[$key][$edit_key]['#bootstrap_components']['tag_button'] = true;
						$element[$key][$edit_key]['#bootstrap_components']['icon'] = '<span class="glyphicon glyphicon-pencil"></span>';
					}
					// move to child
					if(isset($add) || isset($edit)){
						$element[$key]['button_entityconnect'] = array(
							'#weight' => 1,
							'#theme_wrappers' => array('bootstrap_components_container'),
							'#bootstrap_components' => array('input_group_btn' => true),
						);
						if(isset($add)){
							$element[$key]['button_entityconnect'][$add_key] = $element[$key][$add_key];
							unset($element[$key][$add_key]);
						}
						if(isset($edit)){
							$element[$key]['button_entityconnect'][$edit_key] = $element[$key][$edit_key];
							unset($element[$key][$edit_key]);
						}
					}					
					//do selesai, maka kembalikan ke false
					$do = false; 
				}

			}
		}
	}
	// dpm($element,'$element');
}
