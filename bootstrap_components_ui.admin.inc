<?php
// konsep database
/* 
bootstrap_components_form_registry = ';'; // saat kosong
+= $value . ';';
bootstrap_components_form_registry = ';article_node_form;';
bootstrap_components_form_registry = ';article_node_form;page_node_form;';
bootstrap_components_form_registry = ';article_node_form;page_node_form;webform_node_form;';
preg_match('/;$value;/')

 */
function bootstrap_components_ui_admin() {
	$all_types = node_type_get_names();
	// dpm($all_types);
	$header = array(
		t('Name'),
		t('Style'),
		t('Operations'),
	);
	$rows = array();
	foreach ($all_types as $key => $type) {
		$columns = array();
		$columns[] = $type;
		$_variable = 'bootstrap_components_form_' . $key . '_node_form';
		// dvm($_variable);
		$variable = variable_get($_variable);
		// dvm($variable);
		$value = is_null($variable) ? '-' : $variable;
		$columns[] = $value;
		$columns[] = l(t('edit'), 'admin/config/user-interface/bootstrap_components/manage/' . $key . '_node_form');
		$rows[] = $columns;
	}	
	$build['content_type_groups_table'] = array(
		'#theme'  => 'table',
		'#header' => $header,
		'#rows'   => $rows,
		'#empty'  => t('No content type available. <a href="@link">Add content type group</a>.', array('@link' => url('admin/structure/types/add'))),
	);
	return $build;
}



function bootstrap_components_manage_form($form, &$form_state, $id=NULL){
	// dpm($id);
	$form['test'] = array(
		'#markup' => 'Pilih style:',
	);
	$form['pilih'] = array(
		'#type' => 'select',
		'#options' => array(
			'_none' => '-',
			'basic_default' => 'Basic Default',
			'table_default' => 'Table Default',
		),
	);
	$form['target_form_id'] = array(
		'#type' => 'hidden',
		'#value' => $id,
		
	);
	// Action buttons
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['delete'] = array(
    '#type'  => 'button',
    '#value' => t('Delete'),
  );
  $form['actions']['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/config/user-interface/bootstrap_components'),
  );
	return $form;
}
function bootstrap_components_manage_form_submit($form, &$form_state){
	// dpm($form_state,'$form_state');
	
	$target_form_id = $form_state['values']['target_form_id'];
	$pilih = $form_state['values']['pilih'];
	$variable = 'bootstrap_components_form_' . $target_form_id;
	if($pilih != '_none'){
		variable_set($variable,$pilih);	
	}
	else{
			variable_del($variable);	
	}
	drupal_set_message('saved');
	$form_state['redirect'] = 'admin/config/user-interface/bootstrap_components';
	
	
	// return $form;
}