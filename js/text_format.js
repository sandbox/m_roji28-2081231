(function ($) {

/**
 * Automatically display the guidelines of the selected text format.
 */
Drupal.behaviors.bsFilterGuidelines = {
  attach: function (context) {
	
    // $('.bs-text-format', context).once('bs-text-format')
      // .bind('click', function () {
        // $(this).closest('.filter-wrapper')
          // .find('.filter-guidelines').toggle('scroll');
      // });
	  $('.filter-guidelines', context).once('bs-text-format')
      .hide()
      .closest('.filter-wrapper').find('.bs-text-format')
      .bind('click', function () {
       $(this).closest('.filter-wrapper')
          .find('.filter-guidelines').toggle('scroll');
      });
      
	  
	  
	  
  }
};

})(jQuery);